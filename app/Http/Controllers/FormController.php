<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class FormController extends Controller
{

  public function welcome()
  {
    return view('welcome');
  }

  public function index()
  {
    $form = User::all();
    return view('index', compact('form'));
  }

  public function create()
  {
      return view('create');
  }

  public function store(Request $request)
  {
    User::create($request->all());
    return redirect()->route('form.index');
  }

  public function edit($id)
  {
    $form = User::find($id);
    return view('edit', compact('form'));
  }

  public function update(Request $request, $id)
  {
    $user = User::find($id);
    $user->update($request->all());
    return redirect()->route('form.index');
  }

  public function destroy($id)
  {
    User::destroy($id);
    return redirect()->route('form.index');
  }

  public function search(Request $request)
  {
    $form = User::where('phone', 'like', "%$request->phone%")->get();
    return view('index', compact('form'));
   }
}
