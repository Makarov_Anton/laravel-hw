<?php

Route::get('/', 'FormController@welcome')->name('welcome');
Route::get('form/search', 'FormController@search')->name('form.search');
Route::resource('form', 'FormController');

