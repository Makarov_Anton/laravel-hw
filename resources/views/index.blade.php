@extends('layouts.app')
@section('container')


    <form action="{{ route('form.search') }}">
        <input class="input_create" type="text" name="phone" placeholder="Введите номер для поиска">
        <input type="submit" value="search">
    </form>

    <table class="item">
        <tr>
            <th>№п/п</th>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Отчество</th>
            <th>Телефон</th>
            <th>Edit</th>
        </tr>

        @forelse ($form as $item)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $item->surname }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->patronymic }}</td>
                <td>{{ $item->phone }}</td>
                <td>
                    <form action="{{ route('form.edit', $item->id) }}">
                        <input type="submit" value="edit">
                    </form>
                    <form action="{{ route('form.destroy', $item->id) }}" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" value="delete">
                    </form>
                </td>
            </tr>
        @empty
            <h2>Такого номера не существует</h2>
        @endforelse
        <hr>
    </table>
    <a href="{{ route('form.create') }}">Добавить контакт в таблицу </a>
@endsection
