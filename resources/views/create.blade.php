@extends('layouts.app')
@section('container')
    <h2>Введите данные новой записи</h2>
    <form action="{{ route('form.store') }}" method="POST">
        {{ csrf_field() }}
        Фамилия
        <input class="input_create" type="text" name="surname" required><br>
        Имя
        <input class="input_create" type="text" name="name" required><br>
        Отчество
        <input class="input_create" type="text" name="patronymic" required><br>
        Телефон
        <input class="input_create" type="text" name="phone" required><br>
        <input class="input_create" type="hidden" name="id" required><br>
        <input class="input_create" type="submit" value="добавить запись">
    </form>
@endsection