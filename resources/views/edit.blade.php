@extends('layouts.app')
@section('container')
    <h2>Пожалуйста, отредактируйте нужные Вам данные</h2>
    <form action="{{ route('form.update', $form->id) }}" method="post" >
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        Фамилия
        <input class="input_create" type="text" name="surname" value="{{ $form->surname }}" required><br>
        Имя
        <input class="input_create" class="input_create" type="text" name="name" value="{{ $form->name }}" required><br>
        Отчество
        <input class="input_create" type="text" name="patronymic" value="{{ $form->patronymic }}" required><br>
        Телефон
        <input class="input_create" type="text" name="phone" value="{{ $form->phone }}" required><br>
        <input class="input_create" type="hidden" name="id" value="{{ $form->id }}" required><br>
        <input class="input_create" type="submit" value="обновить запись">
    </form>
@endsection