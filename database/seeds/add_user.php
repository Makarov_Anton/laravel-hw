<?php

use Illuminate\Database\Seeder;

class add_user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'id' => 0,
        'surname' => 'Пупкин',
        'name' => 'Василий',
        'patronymic' => 'Леонидович',
        'phone' => '84957776655',
      ]);
    }
}
